
#################################
#--  Validación de Quanto   --#
#################################

data<-read.delim("clipboard", header = T)
head(data)



## Gráficos

##INGRESO  -------------------------------------------------------- 
real<-ggplot(data, aes(x=data$ING_REAL))+
  geom_histogram(breaks=seq(0, max(data$ING_REAL), by = 300), col="blue",fill="lightblue")+
  labs(title="Distribución de Ingreso data") +
  labs(x="Ingreso_data", y="Cantidad") 
  #+ facet_grid(data$PRODUCTO ~ .)

real1<-ggplot(data, aes(x=data$ING_REAL))+
  geom_histogram(breaks=seq(0, 5000, by = 300), col="blue",fill="lightblue")+
  labs(title="Distribución de Ingreso validado") +
  labs(x="Ingreso_data", y="Cantidad") 
#+ facet_grid(data$PRODUCTO ~ .)




##INGRESO QUANTO --------------------------------------------------------  
qto<-ggplot(data, aes(x=data$QUANTO))+
  geom_histogram(breaks=seq(0, max(data$QUANTO), by = 300), col="red",fill="pink")+
  labs(title="Distribución de Quanto") +
  labs(x="Quanto", y="Cantidad") 
   #+ facet_grid(data$PRODUCTO ~ .)

qto1<-ggplot(data, aes(x=data$QUANTO))+
  geom_histogram(breaks=seq(0, 5000, by = 300), col="red",fill="pink")+
  labs(title="Distribución de Quanto") +
  labs(x="Quanto", y="Cantidad") 
#+ facet_grid(data$PRODUCTO ~ .)


library(gridExtra)                                                                                                                                            
pfin1 = grid.arrange(real1,qto1, nrow = 2)

#BoxPlot
p = ggplot(data=data) 
p_1 = p + geom_boxplot(aes(y=data$QUANTO)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
p_2 = p + geom_boxplot(aes(y=data$ING_REAL)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

pfin2 = grid.arrange(p_1,p_2, nrow = 2)
#################################
#-- Métodos de validación  --#
#################################

##### Deciles:


# data$ING_REAL_Rango<- cut(data$ING_REAL, quantile(dev$SCORE_M1,(0:10)/10),include.lowest = TRUE)
data$ING_REAL_Rango<- cut(data$ING_REAL, quantile(data$ING_REAL,(0:10)/10),include.lowest = TRUE,dig.lab=10)
#data$QUANTO_Rango<- cut(data$QUANTO, quantile(data$QUANTO,(0:10)/10),include.lowest = TRUE,dig.lab=10)

data$ING_REAL_Rango<- cut(data$ING_REAL, breaks = c(-Inf,900,1115,1377,1600,2145,2900,3545,4785,8000,+Inf),include.lowest = TRUE,dig.lab=10)
data$QUANTO_Rango<- cut(data$QUANTO, breaks = c(-Inf,900,1115,1377,1600,2145,2900,3545,4785,8000,+Inf),include.lowest = TRUE,dig.lab=10)

table(data$ING_REAL_Rango,data$QUANTO_Rango) 
barplot(table(data$ING_REAL_Rango))
barplot(table(data$QUANTO_Rango))


##### Diferencias puntuales: 

# Quanto < Real -- Subestima| Quanto > Real -- Sobreestima| Quanto = Real -- Coincidencia  :

data$DP<-(data$QUANTO-data$ING_REAL)
data$DP_abs<-abs(data$QUANTO-data$ING_REAL)
data$DP_Sub_Sobr<- ifelse(data$DP<0, "QSubR", ifelse(data$DP>0,"QSobrR","0"))
table(data$DP_Sub_Sobr)
#Breaks:
data$DP_Rango<- cut(abs(data$DP), breaks = c(-Inf,50,100,300,500,1000,1200,2000,3000,4000,Inf),include.lowest = TRUE,dig.lab=10)
table(data$DP_Rango)


##### Intervalos: Quanto +/- n%*Quanto :

#Deciles:


data$Quanto_I25_Inf<-data$QUANTO*(1-0.25)
data$Quanto_I25_Sup<-data$QUANTO*(1+0.25)
data$Quanto_I30_Inf<-data$QUANTO*(1-0.3)
data$Quanto_I30_Sup<-data$QUANTO*(1+0.3)
data$Quanto_I35_Inf<-data$QUANTO*(1-0.35)
data$Quanto_I35_Sup<-data$QUANTO*(1+0.35)
data$Quanto_I40_Inf<-data$QUANTO*(1-0.4)
data$Quanto_I40_Sup<-data$QUANTO*(1+0.4)
data$Quanto_I45_Inf<-data$QUANTO*(1-0.45)
data$Quanto_I45_Sup<-data$QUANTO*(1+0.45)


data$Int_25 <- ifelse(data$ING_REAL < data$Quanto_I25_Inf,"Q_Sobre_R",
                            ifelse(data$ING_REAL >= data$Quanto_I25_Inf & data$ING_REAL<= data$Quanto_I25_Sup,"Coincide",
                                   ifelse(data$ING_REAL > data$Quanto_I25_Sup,"Q_Sub_R","-1")
                                   )
                                )

data$Int_30 <- ifelse(data$ING_REAL < data$Quanto_I30_Inf,"Q_Sobre_R",
                      ifelse(data$ING_REAL >= data$Quanto_I30_Inf & data$ING_REAL<= data$Quanto_I30_Sup,"Coincide",
                             ifelse(data$ING_REAL > data$Quanto_I30_Sup,"Q_Sub_R","-1")
                                  )
                                )

data$Int_35 <- ifelse(data$ING_REAL < data$Quanto_I35_Inf,"Q_Sobre_R",
                      ifelse(data$ING_REAL >= data$Quanto_I35_Inf & data$ING_REAL<= data$Quanto_I35_Sup,"Coincide",
                             ifelse(data$ING_REAL > data$Quanto_I35_Sup,"Q_Sub_R","-1")
                                  )
                                )   


data$Int_40 <- ifelse(data$ING_REAL < data$Quanto_I40_Inf,"Q_Sobre_R",
                      ifelse(data$ING_REAL >= data$Quanto_I40_Inf & data$ING_REAL<= data$Quanto_I40_Sup,"Coincide",
                             ifelse(data$ING_REAL > data$Quanto_I40_Sup,"Q_Sub_R","-1")
                                  )
                                )  

data$Int_45 <- ifelse(data$ING_REAL < data$Quanto_I45_Inf,"Q_Sobre_R",
                      ifelse(data$ING_REAL >= data$Quanto_I45_Inf & data$ING_REAL<= data$Quanto_I45_Sup,"Coincide",
                             ifelse(data$ING_REAL > data$Quanto_I45_Sup,"Q_Sub_R","-1")
                                  )
                                )   


##### Ratio: Quanto/ Ingreso validado:

str(data)
getwd()
write.csv(data,"data_val_qto_prueba1.csv")




